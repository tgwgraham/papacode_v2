from quot import read_config, track_file
#import glob
from os.path import exists, splitext
import toml
from papa_utils import read_config, list_movies
import sys

def runTracking(configfile):


    columns = ['y','x','I0','bg','y_err','x_err','I0_err','bg_err','H_det','error_flag', 
          'snr','rmse','n_iter','y_detect','x_detect','frame','loc_idx','trajectory',  
          'subproblem_n_traj','subproblem_n_locs']

    config = read_config(configfile)
    movies = list_movies(config)

    for f in movies:
        try:
            currbasefname = splitext(f)[0]
            if exists('%s.csv' % currbasefname):
                print('%s.csv already tracked. Skipping.' % currbasefname)
                continue
            if not exists(f):
                raise Exception('File %s not found.' % f)
            trajs = track_file(f, **config)
            print(trajs)
            trajs.to_csv('%s.csv' % currbasefname, index=False, columns=columns)
        except BaseException as err:
            print('Had a problem with %s' % currbasefname)
            print(err)


      
if __name__ == "__main__":
    runTracking(sys.argv[1])